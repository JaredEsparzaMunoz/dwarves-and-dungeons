﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum EnumCardType
{
    Skill, Attack, Defense, Item, Escape
}
public  class Player
{
    public string objectType;
    public int lvl { get; set; }=1;
    public string name { get; set; }
    public int maxHP { get; set; }
    public int currentHP { get; set; }
    public int maxMana { get; set; }
    public int currentMana { get; set; }
    public int strength { get; set; }
    public int intelligence { get; set; }
    public int defense { get; set; }
    public int dexterity { get; set; }
    public int evade { get; set; }
    public int inventoryCapacity { get; set; }
    public int gold { get; set; }

    public HashSet<Skill> skillSet { get; set; }
    public List<Items> lInventario { get; set; }
    public Weapon weapon { get; set; }
    public Armor armor { get; set; }
    public Accesory accesory { get; set; }
    public List<Card> deck { get; set; } = new List<Card>();
       
}
public class Skill
{
    public string name { get; set; }
    public int modifier { get; set; }
    public double chance { get; set; }
}
public class Card
{//skill,ataque,defensa,item,escape

    public EnumCardType CardType{get;set;}
    public string name { get; set; }
    public int modifier { get; set; }
    public double chance { get; set; }

}
public class Items
{
    public string name { get; set; }
    public int modifier { get; set; }
    public int modifier2 { get; set; }
    public int buyrice { get; set; }
    public int sellprice { get; set; }

}
public class Weapon : Items
{ 
}
public class Armor: Items
{
}
public class Accesory: Items 
{
}
public class Consumable : Items
{
}
