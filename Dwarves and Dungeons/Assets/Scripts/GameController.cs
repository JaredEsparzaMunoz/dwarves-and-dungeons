﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Player;


public class GameController : MonoBehaviour
{
    public GameObject charSelectCanv;
    public GameObject mainMenuCanv;
    public GameObject mapCanv;

    public Player p { get; set; }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    public void StartGame()
    {
        mainMenuCanv.SetActive(false);
        charSelectCanv.SetActive(true);
    }

    public void FromSelectToMenu()
    {
        charSelectCanv.SetActive(false);
        mainMenuCanv.SetActive(true);
    }
    public void CharSelection(string n)
    {
        switch (n)
        {
            case "1":
                p = new Player();
                p.name = "Guerrero Amancio";
                break;
            case "2":
                p = new Player();
                p.name = "Arquero Manolo";
                break;
            case "3":
                p = new Player();
                p.name = "Mago Rodrigo";
                break;
        }
        charSelectCanv.SetActive(false);
        mapCanv.SetActive(true);

    }
}
