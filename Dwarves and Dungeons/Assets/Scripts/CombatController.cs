﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static System.Random;
using static Player;


public class CombatController : MonoBehaviour
{
    public GameObject game;

    public Player player;
    public static Random rng = new Random();
    public List<Card> lDeck = new List<Card>();
    public List<Card> lDiscard = new List<Card>();
    public List<Card> lHand = new List<Card>();



    // Start is called before the first frame update
    void Start()
    {
        player = game.GetComponent<GameController>().p;
        player = new Player();
        for (int i =0; i<10; i++)
        {
            Card c = new Card();
            c.name = "Carta Ejemplo " + i.ToString();
            player.deck.Add(c);

        }
        
        lDeck.AddRange(Shuffle(player.deck));
        lHand = FirstHand(lHand,lDeck);


    }
    // Update is called once per frame
    void Update()
    {

    }

    public static List<T> Shuffle<T>(List<T> list)
    {
        System.Random rnd = new System.Random();
        for (int i = 0; i < list.Count; i++)
        {
            int k = rnd.Next(0, i);
            T value = list[k];
            list[k] = list[i];
            list[i] = value;
        }
        return list;
    }
    public static List<Card> FirstHand(List<Card> lHand,List<Card> lDeck)
    {
        for (int i = 0; i < 5; i++)
        {
            lHand.Add(lDeck[i]);
            lDeck.Remove(lDeck[0]);
            Debug.Log(lHand[i].name);
        }
        return lHand;
    }
    public void turn(List<Card> lHand, List<Card> lDeck, List<Card> lDiscard)
    {
        bool victory = false;
        bool gameover = false;
        bool playerTurn = true;
        while (!victory && !gameover)//neither victory or gameover 
        {
            if (playerTurn)//player turn begins
            {
                RefillHand(lHand,lDeck);//check hand and refill if needed

            }
        }
    }
    public static bool FullHand(List<Card> lHand)
    {
        if (lHand.Count < 5)
        {
            return true;
        }
        else return false;
    }
    public static void DrawCard(List<Card> lHand, List<Card> lDeck)
    {
        lHand.Add(lDeck[0]);
        lDeck.RemoveAt(0);
       
    }
    public static void RefillHand(List<Card> lHand, List<Card> lDeck)
    {
        while (FullHand(lHand) != true)
        {
            DrawCard(lHand, lDeck);
        }
    }
}
