﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapController : MonoBehaviour
{
    public GameObject game;
    public Text txtPlayer;
    // Start is called before the first frame update
    void Start()
    {
        txtPlayer.text = game.GetComponent<GameController>().p.name;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
